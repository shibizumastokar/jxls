package ttn.example.jxls.service;

import org.apache.poi.util.IOUtils;
import org.joda.time.DateTime;
import org.jxls.common.Context;
import org.jxls.reader.ReaderBuilder;
import org.jxls.util.JxlsHelper;
import org.springframework.stereotype.Service;
import ttn.example.jxls.model.Student;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by ttn on 29/5/17.
 */

@Service
public class StudentService
{

  // Reading data from excel file using the configuration file

  public Map<String, Student> importRecords() throws Exception
  {
    Map<String, Student> map = new HashMap<>();
    Map<String, ArrayList> beans = new HashMap<>();
    beans.put("students", new ArrayList<Student>());
    fileProcessor(beans);
    for (Object student : beans.get("students"))
    {
      Student tmp = (Student) student;
      map.put(tmp.getAdmissionNumber(), tmp);
    }
    return map;
  }

  // Writing students to excel sheet

  public String exportRecords() throws Exception
  {
    List<Student> students = prepareStudentList();
    String filename = Paths.get(System.getProperty("user.dir")).resolve(UUID.randomUUID().toString() + ".xls").toString();
    try (InputStream is = StudentService.class.getClassLoader().getResourceAsStream("student.xls"))
    {
      try (OutputStream os = new FileOutputStream(filename))
      {
        Context context = new Context();
        context.putVar("students", students);
        JxlsHelper.getInstance().processTemplate(is, os, context);
      }
    }
    return filename;
  }

  // Loading configuration to convert excel data into equivalent Java class

  private void fileProcessor(Map<String, ArrayList> beans) throws Exception
  {
    ReaderBuilder.buildFromXML(StudentService.class.getClassLoader().getResourceAsStream("student.xml"))
                 .read(new ByteArrayInputStream(IOUtils.toByteArray(StudentService.class.getClassLoader().getResourceAsStream("records.xlsx"))), beans);
  }

  // Preparing the list of students which will get exported to excel sheet

  private List<Student> prepareStudentList()
  {
    List<Student> list = new ArrayList<>(5);
    Student student = new Student();
    student.setAdmissionNumber("1");
    student.setAdmissionDate((new DateTime(2017, 02, 05, 00, 00)).toDate());
    student.setCenterCode("SEC47");
    student.setDob((new DateTime(2005, 03, 15, 00, 00)).toDate());
    student.setFirstName("Rei");
    student.setLastName("Chang");
    student.setGender("Female");
    student.setNationality("Chinese");
    list.add(student);
    student = new Student();
    student.setAdmissionNumber("2");
    student.setAdmissionDate((new DateTime(2017, 3, 25, 0, 0)).toDate());
    student.setCenterCode("SEC47");
    student.setDob((new DateTime(2004, 6, 20, 0, 0)).toDate());
    student.setFirstName("Sasuke");
    student.setLastName("Uchiha");
    student.setGender("Male");
    student.setNationality("Japanese");
    list.add(student);
    student = new Student();
    student.setAdmissionNumber("3");
    student.setAdmissionDate((new DateTime(2017, 3, 24, 0, 0)).toDate());
    student.setCenterCode("SEC47");
    student.setDob((new DateTime(2005, 8, 20, 0, 0)).toDate());
    student.setFirstName("Advik");
    student.setLastName("Madan");
    student.setGender("Male");
    student.setNationality("Indian");
    list.add(student);
    return list;
  }
}
