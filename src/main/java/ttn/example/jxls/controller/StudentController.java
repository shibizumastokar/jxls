package ttn.example.jxls.controller;

import org.apache.poi.util.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ttn.example.jxls.model.Student;
import ttn.example.jxls.service.StudentService;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.Map;

/**
 * Created by ttn on 29/5/17.
 */
@RestController
@RequestMapping("/api/student/")
public class StudentController
{
  @Autowired
  private StudentService studentService;

  // Call to import data

  @PostMapping(path = "import")
  public ResponseEntity<Map<String, Student>> importRecords() throws Exception
  {
    Map<String, Student> students = studentService.importRecords();
    return new ResponseEntity(students, HttpStatus.OK);
  }

  // Call to create a excel sheet

  @GetMapping(path = "download")
  public void download(HttpServletResponse response) throws Exception
  {
    String filePath = studentService.exportRecords();
    try (BufferedInputStream is = new BufferedInputStream(new FileInputStream(filePath))) {
      response.setContentType("application/vnd.openxml");
      response.setHeader("Content-Disposition", "attachment; filename=output.xls");
      IOUtils.copy(is, response.getOutputStream());
      response.flushBuffer();
    } catch (Exception exp) {
      exp.printStackTrace();
    }
  }
}
