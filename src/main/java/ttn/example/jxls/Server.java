package ttn.example.jxls;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class Server extends SpringBootServletInitializer
{
  public static void main(String[] args)
  {
    ApplicationContext ctx = SpringApplication.run(Server.class, args);
  }

  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder builder)
  {
    return builder.sources(Server.class);
  }
}