package ttn.example.jxls.model;

import java.util.Date;

/**
 * Created by ttn on 29/5/17.
 */
public class Student
{
  private String admissionNumber;
  private String centerCode;
  private Date   admissionDate;
  private String firstName;
  private String lastName;
  private Date   dob;
  private String gender;
  private String nationality;

  public String getAdmissionNumber()
  {
    return admissionNumber;
  }

  public void setAdmissionNumber(String admissionNumber)
  {
    this.admissionNumber = admissionNumber;
  }

  public String getCenterCode()
  {
    return centerCode;
  }

  public void setCenterCode(String centerCode)
  {
    this.centerCode = centerCode;
  }

  public Date getAdmissionDate()
  {
    return admissionDate;
  }

  public void setAdmissionDate(Date admissionDate)
  {
    this.admissionDate = admissionDate;
  }

  public String getFirstName()
  {
    return firstName;
  }

  public void setFirstName(String firstName)
  {
    this.firstName = firstName;
  }

  public String getLastName()
  {
    return lastName;
  }

  public void setLastName(String lastName)
  {
    this.lastName = lastName;
  }

  public Date getDob()
  {
    return dob;
  }

  public void setDob(Date dob)
  {
    this.dob = dob;
  }

  public String getGender()
  {
    return gender;
  }

  public void setGender(String gender)
  {
    this.gender = gender;
  }

  public String getNationality()
  {
    return nationality;
  }

  public void setNationality(String nationality)
  {
    this.nationality = nationality;
  }
}
